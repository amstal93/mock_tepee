from flask import Flask, request, Response
from flask import jsonify
from abc import ABC
from xml.etree import ElementTree
import argparse
import json
import os
import yaml
from contextvars import ContextVar
from colors import *


class ConfigFactory(object):
    """load API routes and parameters from conf files"""

    def __init__(self, arg):
        self.routes = self._conf_loader(arg)
        self.env = os.environ.get('FLASK_ENV', "development")

    @staticmethod
    def _conf_loader(path):
        try:
            with open(path) as conf_file:
                return json.load(conf_file)
        except json.decoder.JSONDecodeError:
            try:
                with open(path) as conf_file:
                    return yaml.load(conf_file.read(), Loader=yaml.FullLoader)
            except:
                raise SyntaxError('not a valid json nor yaml conf file')


class MessageProcessor(ABC):
    """String identification, parsing and logging"""
    @staticmethod
    def content_type_identifier(message):
        if type(message) is dict:
            return (json.dumps(message), "application/json")
        else:
            try:
                ElementTree.fromstring(message)
                return (message, "application/xml")
            except:
                return (message, "text/plain; charset=utf-8")

    @staticmethod
    def color_application_logs(app):
        @app.before_request
        def log_request_info():
            colored_header = ['{} : {}'.format(
                colors.color(i[0], fg="yellow"),
                colors.color(i[1], fg="blue")) for i in request.headers]
            line = '>> <HEADERS> {} <BODY>. {}'.format(" , ".join(
                colored_header), colors.color(request.get_data(), fg="green"))
            app.logger.info(line)


class AppFactory(object):
    """Flask application builder instantiated with a config"""

    def __init__(self, conf):
        self.conf = conf
        self.app = Flask(__name__)

        if conf.env == "development":
            MessageProcessor.color_application_logs(self.app)

    def _create_view(self, route):
        def view():
            method = ContextVar(request.method).name
            typed_msg = MessageProcessor.content_type_identifier(
                self.conf.routes[route][method].get("response", ""))

            return Response(response=typed_msg[0],
                            status=self.conf.routes[route][method]["sc"],
                            content_type=typed_msg[1])

        self.conf.routes[route]["view"] = view

    def _create_resource(self, route):
        self.app.add_url_rule(route,
                              route.replace("/", ""),
                              self.conf.routes[route]["view"],
                              methods=self.conf.routes[route].keys())

    def build(self):
        for route in self.conf.routes:
            self._create_view(route)
            self._create_resource(route)

        return self.app


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("port")
    parser.add_argument("conf_file")
    args = parser.parse_args()

    conf = ConfigFactory(args.conf_file)
    builder = AppFactory(conf)
    app = builder.build()

    app.run(host='0.0.0.0', port=args.port)
